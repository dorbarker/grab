A simple tool for randomly, but order-preservingly grabbing lines from a file. 

**Installing:**

`cargo install --git https://gitlab.com/dorbarker/grab.git`

```
Usage:
    grab [OPTIONS] [SIZE] [INPUT]

Sample rows from a file.

positional arguments:
  size                  Number of rows to sample [1]
  input                 Input file [stdin]

optional arguments:
  -h,--help             show this help message and exit
  -v,--version          Print version
  --no-header           Do not treat the first line as a header
```

**Examples:**

`grab 1000 my_large_data_table.csv`

```bash
url="https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
curl -s $url | grab --no-header 5
```