extern crate rand;

use rand::Rng;

use std::error::Error;
use std::io::{self, Read, Write, BufRead};
use std::fs::File;

type Input = io::BufReader<Box<Read + 'static>>;

const STDIN: &str = "-";

pub fn process(size: &usize, input_location: &str,
               preserve_header: bool) -> Result<(), Box<Error>> {

    let data = read_input(input_location);

    let contents: Vec<String> = data
        .lines()
        .map(|l| l.unwrap())
        .collect();

    let selected_lines = select(size, contents, preserve_header);

    write_output(selected_lines)
        .expect("Error: Unable to write output to stdout");

    Ok(())
}

fn select(size: &usize, contents: Vec<String>, preserve_header: bool) -> Vec<String> {

    let start = preserve_header as usize;
    let end = (&contents).len();

    let mut available: Vec<usize> = (start..end).collect();

    let mut rng = rand::thread_rng();

    rng.shuffle(&mut available);

    let mut selected = if preserve_header {
            let mut v = Vec::with_capacity(size + 1);
                v.push(0);
                v
    } else {
            Vec::with_capacity(*size)
    };

    for (_, elem) in (0..*size).zip(available) {

        selected.push(elem);
    }

    selected.sort();  // preserve input order

    let output_lines = contents
        .into_iter()
        .enumerate()
        .filter(|(i, _)| selected.contains(&i))
        .map(|(_, line)| line)
        .collect();

    output_lines
}

fn read_input(input_location: &str) -> Input {

    let file_error_msg = format!("Error: File {} could not be read",
                                        &input_location);

    let input_file = match input_location {

        STDIN => Box::new(io::stdin()) as Box<Read>,

         _    => {

             let file = File::open(&input_location)
                 .expect(&file_error_msg);

             Box::new(file) as Box<Read>
         }
    };

    io::BufReader::new(input_file)

}

fn write_output(selected_lines: Vec<String>) -> io::Result<()> {

    let stdout = io::stdout();

    let mut handle = stdout.lock();

    let output = selected_lines.join("\n");

    write!(handle, "{}\n", output)?;

    Ok(())

}
