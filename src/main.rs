extern crate grab;

extern crate argparse;
use argparse::{Store, StoreFalse, Print};


fn main() {

    let mut options = Options {
        size: 1,
        input: "-".to_string(),
        preserve_header: true
    };

    {
        let mut parser = argparse::ArgumentParser::new();

        parser.set_description("Sample rows from a file.");

        parser.add_option(&["-v", "--version"],
                          Print(env!("CARGO_PKG_VERSION").to_string()),
                          "Print version");

        parser.refer(&mut options.preserve_header)
            .add_option(&["--no-header"],
                         StoreFalse,
                          "Do not treat the first line as a header");

        parser.refer(&mut options.size)
            .add_argument("size",
                          Store,
                          "Number of rows to sample [1]");

        parser.refer(&mut options.input)
            .add_argument("input",
                          Store,
                          "Input file [stdin]");

        parser.parse_args_or_exit();
    }

    grab::process(&options.size, &options.input, options.preserve_header)
        .unwrap();

}

struct Options {

    size: usize,
    input: String,
    preserve_header: bool
}